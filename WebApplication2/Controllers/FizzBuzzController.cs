﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class FizzBuzzController : Controller
    {
        // GET: FizzBuzz
        public ActionResult Index()
        {
            return View();
        }

        // GET: /FizzBuzz/Licz
        public ActionResult Count()
        {
            Licz l = new Licz();
            return View(l);
        }

        [HttpPost]
        public ActionResult Count(Licz licz)
        {
            LiczContext db = new LiczContext();

            string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ipAddress))
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            }

            licz.ip = ipAddress;

            if (licz.liczba < 1 || licz.liczba > 100)
            {
                licz.wynik = "Nieprawidłowe dane!";
                return View("Out", licz);
            }
            if (licz.liczba % 3 == 0 && licz.liczba % 5 == 0 && licz.liczba % 7 == 0)
            {
                licz.wynik = "FizzBuzzWizz";
                db.LiczbyDB.Add(licz);
                db.SaveChanges();
                return View("Out", licz);
            }
            if (licz.liczba % 5 == 0 && licz.liczba % 7 == 0)
            {
                licz.wynik = "BuzzWizz";
                db.LiczbyDB.Add(licz);
                db.SaveChanges();
                return View("Out", licz);
            }
            if (licz.liczba % 3 == 0 && licz.liczba % 5 == 0)
            {
                licz.wynik = "FizzBuzz";
                db.LiczbyDB.Add(licz);
                db.SaveChanges();
                return View("Out", licz);
            }
            if (licz.liczba % 3 == 0 && licz.liczba % 7 == 0)
            {
                licz.wynik = "FizzWizz";
                db.LiczbyDB.Add(licz);
                db.SaveChanges();
                return View("Out", licz);
            }
            if (licz.liczba % 3 == 0)
            {
                licz.wynik = "Fizz";
                db.LiczbyDB.Add(licz);
                db.SaveChanges();
                return View("Out", licz);
            }
            if (licz.liczba % 5 == 0)
            {
                licz.wynik = "Buzz";
                db.LiczbyDB.Add(licz);
                db.SaveChanges();
                return View("Out", licz);
            }
            if (licz.liczba % 7 == 0)
            {
                licz.wynik = "Wizz";
                db.LiczbyDB.Add(licz);
                db.SaveChanges();
                return View("Out", licz);
            }
            else
            {
                licz.wynik = licz.liczba.ToString("0");
                db.LiczbyDB.Add(licz);
                db.SaveChanges();
                return View("Out", licz);
            }
            
        }
    }
}