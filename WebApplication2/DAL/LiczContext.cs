﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;

namespace WebApplication2.Models
{
    public class LiczContext : DbContext
    {
        public LiczContext(): base("name=LiczbyDB")
        {
            Database.SetInitializer<LiczContext>(new CreateDatabaseIfNotExists<LiczContext>());
        }
        public DbSet<Licz> LiczbyDB { get; set; }
    }
}