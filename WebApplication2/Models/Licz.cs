﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace WebApplication2.Models
{
    public class Licz
    {
        [Display(Name = "Liczba")]
        [Key]
        public int id { get; set; }
        public string ip { get; set; }
        public int liczba { get; set; }
        public string wynik { get; set; }
    }
}